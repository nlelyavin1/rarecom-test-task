from flask import Blueprint, request

from src.services.decorators import JWT_required

bp = Blueprint('api', __name__, url_prefix='/api/v1')


@bp.route('/message/', methods=['POST'])
def message_save():
    # request.headers.get('Authorization')
    print(request.headers)
    return {'a': 'Hello World!'}


@bp.route('/message_confirmation/', methods=['POST'])
@JWT_required
def message_confirmation():
    request.headers.get('Authorization')
    return 'Hello World!'
