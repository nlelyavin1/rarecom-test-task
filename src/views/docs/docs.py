from flask import Blueprint, render_template, send_from_directory, url_for

bp = Blueprint('docs', __name__, url_prefix='')


@bp.route('/v1/docs/', methods=['GET'])
def swagger_ui():
    return render_template('docs/swagger_ui.html')


@bp.route('/spec')
def get_spec():
    return send_from_directory(bp.root_path, 'static/open_api.yaml')
