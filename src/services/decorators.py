import functools

from flask import Response


def JWT_required(view):

    @functools.wraps(view)
    def wrapped_view(**kwargs):
        response = ''

        if kwargs.get('request').headers:
            response = Response('You dont have the JWT token')

        response = view(**kwargs)
        return response
    return wrapped_view
