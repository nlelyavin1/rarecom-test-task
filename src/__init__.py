from flask import Flask


def create_app():
    app = Flask(__name__)

    from .views import api, docs

    app.register_blueprint(api.bp)
    app.register_blueprint(docs.bp)

    return app
