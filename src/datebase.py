"""
Script with config and models DB
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base

from config import DATABASE_URI


class DBWorker:
    def __init__(self):
        self.__engine = create_engine(DATABASE_URI, echo=True)
        self.__declarative_base = declarative_base()

    def init_db(self) -> None:
        self.__declarative_base.metadata.create_all(bind=self.__engine)

    def get_base(self):
        return self.__declarative_base


db_worker = DBWorker()

db_declarative_base = db_worker.get_base()


class Message(db_declarative_base):
    __tablename__ = 'message'
